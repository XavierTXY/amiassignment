import java.util.*;

public class Main {

	public static void main(String args[]) {


		ArrayList<Node> allNodes = new ArrayList<Node>();

		Node n1 =  new Node( "n1" );
		Node n2 =  new Node( "n2" ); 
		Node n3 =  new Node( "n3" );
		Node n4 =  new Node( "n4" );
		Node n5 =  new Node( "n5" );

		Node n6 =  new Node( "n6" );

		n1.addAdjDetails( n2, 2 );
		n1.addAdjDetails( n3, 5 );
		n1.addAdjDetails( n4, 10 );
		n1.addAdjDetails( n5, 20 );

		n2.addAdjDetails( n6, 200 );

		allNodes.add( n1 );
		allNodes.add( n2 );
		allNodes.add( n3 );
		allNodes.add( n4 );
		allNodes.add( n5 );
		allNodes.add( n6 );

		n2.setDistanceToGoal( 24.3 );
		n3.setDistanceToGoal( 10.3 );
		n4.setDistanceToGoal( 90.3 );
		n5.setDistanceToGoal( 2.3 );
		n6.setDistanceToGoal( 101 );


		//System.out.println(n1.toString());
		
		ArrayList<Node> nodes = n1.getAdjNode();
		System.out.println(nodes.size());

		
		
		for( int i = 1; i <= n1.getNumNodes(); i++ ) {
			Node nn = allNodes.get(i);
			nn.calcHValue(n1.getAdjDistance(i-1));
			System.out.println(nn.toString());

		}

		int i = 1;
		for( Node n : nodes ) {
			//n.calcHValue(n1.getAdjDistance(i));
			System.out.println(n.toString());
			i++;
		}


	}


}