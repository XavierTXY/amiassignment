import java.util.*;

public class Node {

	private String name;
	private double distanceToGoal;
	private double hValue;
	private ArrayList<Node> adjNode;
	private ArrayList<Integer> adjDistance;

	public Node( String inName ) {

		setName( inName );
		hValue = 0.0;
		distanceToGoal = 0.0;
		adjNode = new ArrayList<Node>();
		adjDistance = new ArrayList<Integer>();
	}

	private void setName( String inName ) {

		if( inName.equals("") || inName == null ) {

			throw new IllegalArgumentException( "Node's name is invalid" );

		} else {

			name = inName;
		}

	}

	public void setHValue( double inHValue ) {

		hValue = inHValue;
	}

	public void setDistanceToGoal( double inDistance ) {

		distanceToGoal = inDistance;
	}

	public void addAdjDetails( Node inNode, int inDistance ) {

		if( inNode == null || inDistance <= 0 ) {

			throw new IllegalArgumentException( "Invalid Node's detail is added" );
		} else {

			adjNode.add( inNode );
			adjDistance.add( inDistance );
		}
		
	}

	public String getName() {
		return name;
	}

	public double getHValue() {
		return hValue;
	}

	public double getDistanceToGoal() {
		return distanceToGoal;
	}

	public ArrayList<Node> getAdjNode() {
		return adjNode;
	}

	public int getAdjDistance( int index ) {
		return adjDistance.get(index);
	}

	public int getNumNodes() {

		return adjNode.size();
	}

	public void calcHValue( int inDistance ) {
		hValue = distanceToGoal + inDistance;
	}
	public String toString() {

		return new String( "Name: " + name + ", Distance to goal: " + distanceToGoal + ", H value: " + hValue );
	}


}