public class Edge {

	private int name;
	private int distance;
	private Node leftNode;
	private Node rightNode;

	public Edge( int inName, int inDistance, Node inLeftNode, Node inRightNode ) {

		name = inName;
		setDistance( inDistance );
		setLeftNode( inLeftNode );
		setRightNode( inRightNode );
	}

	public void setDistance( int inDistance ) {

		if( inDistance <= 0 ) {

			throw new IllegalArgumentException( "Distance can't be less than or equal to 0" );
		}
	}

	private void checkInvalidNode( Node inNode ) {

		if( inNode == null ) {

			throw new IllegalArgumentException( "Node is null");
		}
	}

	public void setLeftNode( Node inLeftNode ) {

		checkInvalidNode( inLeftNode );

		leftNode = inLeftNode;
	}

	public void setRightNode( Node inRightNode ) {

		checkInvalidNode( inRightNode );

		rightNode = inRightNode;
	}

	public int getName() {

		return name;
	}

	public int getDistance() {

		return distance;
	}

	public Node getLeftNode() {

		return leftNode;
	}

	public Node getRightNode() {

		return rightNode;
	}

}